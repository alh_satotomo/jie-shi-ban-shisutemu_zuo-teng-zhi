package sato_tomo.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import sato_tomo.beans.UserMessage;
import sato_tomo.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(category) == false) {
			category = new String(category.getBytes("8859_1"), "UTF-8");
		}

		List<UserMessage> messages = new MessageService().getMessage(startDate,endDate,category);

		request.setAttribute("messages",messages);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}