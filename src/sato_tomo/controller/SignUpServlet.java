package sato_tomo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sato_tomo.beans.User;
import sato_tomo.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User users = new User();

		if (isValid(request, messages) == true) {

			users.setAccount(request.getParameter("account"));
			users.setPassword(request.getParameter("password"));
			users.setName(request.getParameter("name"));
			users.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			users.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

			new UserService().register(users);

			response.sendRedirect("control");

		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String branch_id_st = request.getParameter("branch_id");
		String position_id_st = request.getParameter("position_id");

		if(StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if(branch_id_st == null) {
			messages.add("支店番号を入力してください");
		}

		if(StringUtils.isEmpty(position_id_st) == true) {
			messages.add("部署・役職番号を入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

