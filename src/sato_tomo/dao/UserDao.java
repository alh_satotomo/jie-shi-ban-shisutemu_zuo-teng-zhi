package sato_tomo.dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import sato_tomo.beans.User;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(",is_working");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", ?");//is_working
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getPosition_id());
			ps.setInt(6, user.getIs_working());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String account,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> getUserInformation(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");
			sql.append("users.is_working as is_working, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date, ");
			sql.append("branches.id as id, ");
			sql.append("branches.name as name, ");
			sql.append("positions.id as id, ");
			sql.append("positions.name as name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id");


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				int is_working = rs.getInt("is_working");
				Timestamp created_Date = rs.getTimestamp("created_date");
				Timestamp updated_Date = rs.getTimestamp("updated_date");

				User users = new User();
				users.setId(id);
				users.setAccount(account);
				users.setPassword(password);
				users.setName(name);
				users.setBranch_id(branch_id);
				users.setPosition_id(position_id);
				users.setIs_working(is_working);
				users.setCreated_date(created_Date);
				users.setUpdated_date(updated_Date);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			if(!user.getPassword().isEmpty()) {
				sql.append("UPDATE users SET");
				sql.append("  account = ?");
				sql.append(", name = ?");
				sql.append(", password = ?");
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", updated_date = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranch_id());
				ps.setInt(5, user.getPosition_id());
				ps.setInt(6, user.getId());
			}else {
				sql.append("UPDATE users SET");
				sql.append("  account = ?");
				sql.append(", name = ?");
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", updated_date = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranch_id());
				ps.setInt(4, user.getPosition_id());
				ps.setInt(5, user.getId());
			}


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void UserWorking(Connection connection,User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			if(user.getIs_working() == 0) {
				sql.append("UPDATE users SET ");
				sql.append("is_working=1 ");
				sql.append("WHERE ");
				sql.append("id = ?");
				// sql.append("WHERE id = ?");


			}

			if(user.getIs_working() == 1) {
				sql.append("UPDATE users SET ");
				sql.append("is_working=0 ");
				sql.append("WHERE ");
				sql.append("id = ?");
				// sql.append("WHERE id = ?");
			}

			ps = connection.prepareStatement(sql.toString());


			ps.setInt(1, user.getId());

			ps.executeUpdate();


		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}